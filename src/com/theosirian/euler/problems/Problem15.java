package com.theosirian.euler.problems;

public class Problem15 {

	public static long answer() {

		/* Imagine the 20x20 grid, in the starting point, if you go right you write 0, if you go down you write 1 
		 * In the end, no matter which way you make, you would end up writing 20 0s and 20 1s
		 * Eg. straight right then straight down would be '0000000000000000000011111111111111111111'
		 * And if you choose straight down then straight right you get the opposite: '1111111111111111111100000000000000000000'
		 * Now that we know that we will write 20 1s and 20 0s no matter what, we use the permutation formula to find the number
		 * of possible combinations with repeated numbers. 
		 * In case you don't know it: P<A1,A2,...An>(N) = (N!)/(A1!*A2!* ... An!)
		 * Where <A1,A2,...An> are the number of repetitions of a symbol/letter/number and N is the total number of symbols/letters/numbers
		 * So for this case it would be:
		 * P<20, 20> (40)
		 * Because we have 20 repeating 1s and 20 repeating 0s in a total of 40 numbers.
		 * Now i just implement it in code.
		 * Since we are working with square grids, the side will give us all we need.
		 * So if you change it to 2 you can see it will output 6 as the example in the site states.
		 * **This algorithm only works with square grids**
		*/

		double squareGridSideSize = 20;

		// int is too short for the values, so I used double, then I cast it to long for sake of reading and copy/paste.

		return (long) ((factorial(squareGridSideSize * 2)) / (factorial(squareGridSideSize) * factorial(squareGridSideSize)));

	}

	public static double factorial(double number) {

		// Quick recursive factorial function.

		if (number == 1) {
			return 1;
		} else {
			return number * factorial(number - 1);
		}
	}
}
