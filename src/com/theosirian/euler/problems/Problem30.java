package com.theosirian.euler.problems;

/**
 * Created by SirOsirian on 29/12/13.
 */
public class Problem30 {

	public static int answer() {

		// I randomly set the limit to 1 million and it worked,
		// but to find the limit all you need to do, as i found
		// out in the answers thread later, you need to check
		// when the number is greater than the sum of 5th power
		// of the digits of the number
		// 9 < 9^5
		// 99 < 2*9^5
		// 999 < 3*9^5
		// 9999 < 4*9^5
		// 99999 < 5*9^5
		// 999999 > 6*9^5
		// so limit is 6*9^5 = 354294
		// Implemented later

		int exp = 5, limit = getLimit(exp), sum, fifthPowers = 0;

		for (int i = 2; i < limit; i++) {

			sum = 0;

			for (int j = 0; j < String.valueOf(i).length(); j++) {
				sum += sumPowerDigits(Character.getNumericValue(String.valueOf(i).charAt(j)), (double) exp);
			}

			if (sum == i) {
				fifthPowers += i;
			}

		}

		return fifthPowers;

	}

	private static int getLimit(int exp) {

		int test = (int) Math.pow(9.0, (double) exp);
		String nines = "9";

		for (int i = 0; ; i++) {

			if (test * (i + 1) < Integer.parseInt(nines)) {

				return test * (i + 1);

			} else {

				nines += "9";

			}

		}

	}

	public static int sumPowerDigits(int number, double exp) {

		if (number == 0) return 0;
		else if (number == 1) return 1;
		else return (int) Math.pow((double) number, exp);

	}

}
