package com.theosirian.euler.problems;

/**
 * Created by SirOsirian on 29/12/13.
 */
public class Problem27 {

	public static int answer() {

		int highestCount = 0, coefficients = 0;

		for (int a = 1; Math.abs(a) < 1000; a = nextNumber(a)) {

			for (int b = 1; Math.abs(b) < 1000; b = nextNumber(b)) {

				if (a == -79 && b == 1601) System.out.println("Hello");

				int n = 0;

				while (true) {

					if (isPrime(solveQuadratic(n, a, b))) {

						n++;

					} else {

						break;

					}

				}

				if (n > highestCount) {

					highestCount = n;
					coefficients = a * b;
//					System.out.println(highestCount + " from n² " + a + "n + (" + b + ")");

				}

			}

		}

		return coefficients;

	}

	public static int solveQuadratic(int n, int a, int b) {

		return (n * n) + (a * n) + b;

	}

	public static int nextNumber(int previous) {

		if (previous < 0) {

			return Math.abs(previous) + 1;

		} else {

			return 0 - previous;

		}

	}

	public static boolean isPrime(long number) {


		if (number < 2) return false;
		if (number == 2 || number == 3) return true;
		if (number % 2 == 0 || number % 3 == 0) return false;
		long sqrtN = (long) Math.sqrt(number) + 1;

		for (long i = 6L; i <= sqrtN; i += 6) {

			if (number % (i - 1) == 0 || number % (i + 1) == 0) return false;

		}

		return true;

	}

}
