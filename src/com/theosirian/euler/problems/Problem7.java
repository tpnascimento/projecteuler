package com.theosirian.euler.problems;

import java.util.ArrayList;
import java.util.List;

public class Problem7 {

	public static int answer() {

		// what is the 10 001st prime number? (2, 3, 5, 7, 11, 13...)

		List<Integer> primes = new ArrayList<Integer>();

		primes.add(2);
		boolean isPrime;

		for (int n = 3; ; n += 2) {
			isPrime = false;
			for (int i = 2; i < n; i++) {
				if (n % i > 0) {
					isPrime = true;
				} else {
					isPrime = false;
					break;
				}
			}
			if (isPrime) {
				primes.add(n);
			}
			if (primes.size() == 10001) {
				return primes.get(10000);
			}
		}

	}

}
