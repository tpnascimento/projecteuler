package com.theosirian.euler.problems;

/**
 * Created by SirOsirian on 27/12/13.
 */

public class Problem19 {

	// 1 jan 1900 was monday
	// Number of sundays as first day of the month from (1 jan 1901 to 31 dec 2000)
	// Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
	// 1 jan 1901 tuesday
	private static final int STARTING_YEAR = 1901, FINAL_YEAR = 2000;
	private static final int[] monthDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	private static WeekDay weekDay = new WeekDay(Day.MONDAY);

	public static int answer() {

		int total = 0;

		for (int year = STARTING_YEAR; year <= FINAL_YEAR; year++) {

			if (leapYear(year)) {
				monthDays[1] = 29;
			} else {
				monthDays[1] = 28;
			}

			for (int month = 0; month < 12; month++) {
				// Real month number = month+1

				for (int day = 0; day < monthDays[month]; day++) {
					// Real day number = day+1

					weekDay.forward();
					if (day == 0 && weekDay.getDay() == Day.SUNDAY) {
						total++;
					}

				}

			}

		}

		return total;
	}

	private static boolean leapYear(int year) {

		// Taken from wikipedia

		if (year % 400 == 0) {
			return true;
		} else if (year % 100 == 0) {
			return false;
		} else if (year % 4 == 0) {
			return true;
		} else {
			return false;
		}

	}

	static class WeekDay {

		private Day day;

		public WeekDay(Day day) {
			this.day = day;
		}

		public Day getDay() {

			return this.day;
		}

		public void forward() {
			if (day == Day.SATURDAY) this.day = Day.SUNDAY;
			else if (day == Day.SUNDAY) this.day = Day.MONDAY;
			else if (day == Day.MONDAY) this.day = Day.TUESDAY;
			else if (day == Day.TUESDAY) this.day = Day.WEDNESDAY;
			else if (day == Day.WEDNESDAY) this.day = Day.THURSDAY;
			else if (day == Day.THURSDAY) this.day = Day.FRIDAY;
			else if (day == Day.FRIDAY) this.day = Day.SATURDAY;
		}

	}

	public enum Day {
		SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
		THURSDAY, FRIDAY, SATURDAY
	}

}
