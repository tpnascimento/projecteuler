package com.theosirian.euler.problems;

import java.math.BigDecimal;

/**
 * Created by SirOsirian on 28/12/13.
 */
public class Problem26 {

    public static int answer() {

        int higherCount = 0, newCount, denominator = 0;

        for (int i = 1; i < 1000; i++) {

            BigDecimal numerator = new BigDecimal(1);
            BigDecimal denumerator = new BigDecimal(i);

            try {

                BigDecimal periodic = numerator.divide(denumerator);

            } catch (ArithmeticException e) {

                newCount = mod(i, 10);

                if (newCount > higherCount) {

                    higherCount = newCount;
                    denominator = i;

                }

            }

        }

        return denominator;

    }

    public static int mod(int number, int base) {

        for (int i = 1; i < number; i++) {

            BigDecimal power = new BigDecimal(base);
            power = power.pow(i);
            power = power.subtract(new BigDecimal(1));
            BigDecimal test;

            try {

                test = power.divide(new BigDecimal(number));

            } catch (ArithmeticException e) {

                continue;

            }

            return test.toString().length();

        }

        return -1;
    }

}
