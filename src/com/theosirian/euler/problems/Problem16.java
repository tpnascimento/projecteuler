package com.theosirian.euler.problems;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Problem16 {

	public static int answer() {

		// Sum of the digits of 2^1000
		// String multiplication was my first approach to the problem
		// Then someone hinted me to use BigInteger to make it easier
		// Both work as they should

		BigInteger number = new BigInteger("2");

		number = number.pow(1000);

		String longNumber = String.valueOf((long) Math.pow(2, 50));

		// This was the hardest part lol
		// ln^2 = ln * ln = multiply(longNumber, longNumber);
		// ln^4 = ln2 * ln2 = multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber));
		// ln^8 = ln4 * ln4 = multiply(multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber)), multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber)));
		// ln^16 = ln8 * ln8 = multiply(multiply(multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber)), multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber))), multiply(multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber)), multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber))));
		// ln^20 = ln16 * ln4 = multiply(multiply(multiply(multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber)), multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber))), multiply(multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber)), multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber)))), multiply(multiply(longNumber, longNumber),multiply(longNumber, longNumber)));

		System.out.println(sumDigits(multiply(
				multiply(
						multiply(multiply(multiply(longNumber, longNumber), multiply(longNumber, longNumber)),
								multiply(multiply(longNumber, longNumber), multiply(longNumber, longNumber))),
						multiply(multiply(multiply(longNumber, longNumber), multiply(longNumber, longNumber)),
								multiply(multiply(longNumber, longNumber), multiply(longNumber, longNumber)))),
				multiply(multiply(longNumber, longNumber), multiply(longNumber, longNumber)))));

		return sumDigits(number.toString());

	}

	private static int sumDigits(String str) {

		int result = 0;

		for (int i = 0; i < str.length(); i++) {

			result += Character.getNumericValue(str.charAt(i));

		}

		return result;
	}

	private static String sumAll(List<String> strings) {

		int result, extra = 0;
		String holder = "";
		StringBuilder first = new StringBuilder(strings.get(0));
		first.reverse();

		for (int i = 1; i < strings.size(); i++) {

			StringBuilder nextNumber = new StringBuilder(strings.get(i));
			nextNumber.reverse();

			while (nextNumber.length() != first.length()) {

				if (nextNumber.length() < first.length()) {

					nextNumber.ensureCapacity(nextNumber.capacity() + 1);
					nextNumber.append("0");

				}

				if (nextNumber.length() > first.length()) {

					first.ensureCapacity(first.capacity() + 1);
					first.append("0");

				}

			}

			StringBuilder str = new StringBuilder(nextNumber.toString());

			holder = "";

			for (int j = 0; j < str.length(); j++) {

				result = Character.getNumericValue(str.charAt(j)) + Character.getNumericValue(first.charAt(j));
				result += extra;

				String resultStr = String.valueOf(result);

				if (result >= 10) {
					extra = Integer.parseInt(resultStr.substring(0, resultStr.length() - 1));
				} else {
					extra = 0;
				}

				holder = resultStr.charAt(resultStr.length() - 1) + holder;

			}

			holder = extra + "" + holder;
			extra = 0;

			first = new StringBuilder(holder);
			first.reverse();

		}

		return holder;
	}

	private static String multiply(String a, String b) {

		int subTotal, extra;

		String waitingString;
		StringBuilder number1 = new StringBuilder(a);
		StringBuilder number2 = new StringBuilder(b);
		List<String> numbers = new ArrayList<String>();
		number1.reverse();
		number2.reverse();

		for (int i = 0; i < number1.length(); i++) {

			waitingString = "";
			extra = 0;
			subTotal = Character.getNumericValue(number1.charAt(i));

			for (int j = 0; j < number2.length(); j++) {

				subTotal *= Character.getNumericValue(number2.charAt(j));
				subTotal += extra;

				waitingString = String.valueOf(subTotal % 10) + waitingString;

				if (subTotal >= 10 || ((j == number2.length() - 1) && (String.valueOf(subTotal).length() > 1))) {

					extra = subTotal / 10;

				} else {

					extra = 0;

				}

				subTotal = Character.getNumericValue(number1.charAt(i));

			}

			for (int k = 0; k < i; k++) {
				waitingString += "0";
			}

			waitingString = extra + "" + waitingString;

			numbers.add(waitingString);

		}

		return sumAll(numbers);
	}
}