package com.theosirian.euler.problems;

import java.math.BigInteger;

/**
 * Created by SirOsirian on 27/12/13.
 */
public class Problem20 {

	public static int answer() {

		BigInteger number = new BigInteger("100");

		return sumDigits(factorial(number).toString());
	}

	public static BigInteger factorial(BigInteger number) {

		// Quick recursive factorial function.

		if (number.equals(new BigInteger("1"))) {
			return new BigInteger("1");
		} else {
			return number.multiply(factorial(number.subtract(new BigInteger("1"))));
		}
	}

	private static int sumDigits(String str) {

		int result = 0;

		for (int i = 0; i < str.length(); i++) {

			result += Character.getNumericValue(str.charAt(i));

		}

		return result;
	}

}
