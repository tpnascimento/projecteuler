package com.theosirian.euler.problems;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SirOsirian on 29/12/13.
 */
public class Problem29 {

	public static int answer() {

		List<BigInteger> distinctTerms = new ArrayList<BigInteger>();

		for (int a = 2; a <= 100; a++) {

			for (int b = 2; b <= 100; b++) {

				BigInteger number = new BigInteger(String.valueOf(a)).pow(b);

				if (!distinctTerms.contains(number)) {

					System.out.println(number);
					distinctTerms.add(number);

				}

			}

		}

		return distinctTerms.size();

	}

}
