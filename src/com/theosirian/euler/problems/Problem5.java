package com.theosirian.euler.problems;

public class Problem5 {

	public static int answer() {

		// smallest positive number that is evenly divisible by all of the
		// numbers from 1 to 20

		for (int i = 20; ; i += 2) {
			if (i % 7 == 0 && i % 11 == 0 && i % 12 == 0 && i % 13 == 0 && i % 14 == 0
					&& i % 15 == 0 && i % 16 == 0 && i % 17 == 0 && i % 18 == 0
					&& i % 19 == 0 && i % 20 == 0) {
				return i;
			}
		}
	}
}
