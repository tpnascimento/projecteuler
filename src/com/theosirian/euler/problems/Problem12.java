package com.theosirian.euler.problems;

public class Problem12 {

	private static int previousNumber = 0;

	public static int answer() {
		int nextNumber, divisorCounter;
		for (int i = 1; ; i++) {
			divisorCounter = 0;
			nextNumber = triangleNumber(i);

			//System.out.println(nextNumber);

			for (int j = 1; j <= nextNumber; j++) {
				if (nextNumber % j == 0) {
					divisorCounter++;
				}
			}
			if (divisorCounter > 100) {
				System.out.println(divisorCounter);
			}
			if (divisorCounter >= 500) {
				return nextNumber;
			}

		}

	}

	public static int triangleNumber(int number) {

		previousNumber += number;
		return previousNumber;

	}

}
