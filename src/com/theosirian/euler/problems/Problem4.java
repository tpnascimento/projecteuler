package com.theosirian.euler.problems;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

public class Problem4 {

	public static int answer() throws FileNotFoundException,
			UnsupportedEncodingException {

		// lasgest palindromic number that is a product of 2 3-digit numbers
		// (100x100 ~ 999x999)
		// 10000 ~ 998001 (between these 2 numbers)

		int largestSoFar = 0;

		for (int i = 101; i < 999; i++) {

			for (int j = i; j < 999; j++) {

				int mult = i * j;
				StringBuilder reversed = new StringBuilder("" + mult);
				String normal = "" + mult;
				reversed.reverse();

				if (normal.equals(reversed.toString()) && mult > largestSoFar) {

					largestSoFar = mult;

				}

			}

		}

		return largestSoFar;

	}
}
