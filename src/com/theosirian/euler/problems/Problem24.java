package com.theosirian.euler.problems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SirOsirian on 28/12/13.
 */
public class Problem24 {

	// There are 9! combinations for every number we maintain fix at the start
	// 9! = 362880
	// If we multiply by 3, it goes beyond 1 milion, so the 2nd number is the one in the front
	// Since everything is being ordered lexicographically, the order the numbers show
	// in the first position is {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, the second number is 1.
	// So we are looking for a number that starts with 1. Now for the second number, for
	// every number in the second position, we have 8! combinations (40320). Keep adding
	// to 362880 to get as close as possible without going over 1 million. Final result is
	// 967680 after adding to it 6 times. Now we grab the 6th number out of the initial array
	// of number without counting the one we took before (1), so we grab the 6th out of
	// {0, 2, 3, 4, 5, 6, 7, 8, 9}, which is 6. Now we know the millionth number will start
	// with 16--------
	// Keep doing the same process until you find it.
	// Now to implement it as code is a bit more complicated but I'll try
	// 967680 5040 6th {0, 2, 3, 4, 5, 7, 8, 9} 167
	// 997920 720 2nd {0, 2, 3, 4, 5, 8, 9} 1672
	// 999360 120 5th {0, 3, 4, 5, 8, 9} 16728
	// 999960 24 1st {0, 3, 4, 5, 9} 167280
	// 999984 6 2nd {3, 4, 5, 9} 1672804
	// 999996 2 3rd {3, 5, 9} 16728049
	//1000000 0 0th {3, 5}


	public static final int POSITION = 1000000;
	public static List<String> numbers = new ArrayList<String>();

	public static String answer() {

		numbers.add("0");
		numbers.add("1");
		numbers.add("2");
		numbers.add("3");
		numbers.add("4");
		numbers.add("5");
		numbers.add("6");
		numbers.add("7");
		numbers.add("8");
		numbers.add("9");

		String answer = "";
		int limit = numbers.size(), sum = 0, counter;

		for (int i = 1; i < +limit; i++) {

			counter = 0;


			for (; (sum + factorial(limit - i)) < POSITION; ) {

				sum += factorial(limit - i);
				counter++;

			}

			answer += numbers.get(counter);
			numbers.remove(counter);

			if (i == limit - 1) {
				answer += numbers.get(0);
				break;
			}

		}

		return answer;
	}

	public static int factorial(int number) {

		// Quick recursive factorial function.

		if (number == 1) {
			return 1;
		} else {
			return number * factorial(number - 1);
		}
	}

}
