package com.theosirian.euler.problems;

public class Problem9 {

	public static int answer() {

		// A Pythagorean triplet is a set of three natural numbers, a < b < c,
		// for which,
		// a2 + b2 = c2
		// There exists exactly one Pythagorean triplet for which a + b + c =
		// 1000.
		// Find the product abc.

		for (int i = 1; i <= 1000; i++) {

			for (int j = 2; j <= (1000 - i); j++) {

				for (int k = 3; k <= (1000 - (i + j)); k++) {

					if (Math.pow(i, 2) + Math.pow(j, 2) == Math.pow(k, 2)
							&& i < j && j < k) {

						System.out.println(i + "," + j + ", " + k);

						int sum = i + j + k;

						if (sum == 1000) {
							return i * j * k;
						}

					}

				}

			}

		}
		return -1; // just because eclipse asked.
	}
}
