package com.theosirian.euler.problems;

import java.math.BigInteger;

/**
 * Created by SirOsirian on 28/12/13.
 */
public class Problem25 {

	public static String answer() {

		BigInteger thousandDigits = new BigInteger("1");
		BigInteger previousNumber = new BigInteger("1");

		for (int i = 2; thousandDigits.toString().length() <= 1000; i++) {

			if (thousandDigits.toString().length() >= 1000) {

				System.out.println(i + "th term: " + thousandDigits.toString() + " is the first with 1000 digits.");
				return thousandDigits.toString();

			} else {

				BigInteger addMe = previousNumber;
				previousNumber = thousandDigits;
				thousandDigits = thousandDigits.add(addMe);

			}

		}

		return null;

	}
}
