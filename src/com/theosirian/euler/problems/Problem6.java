package com.theosirian.euler.problems;

public class Problem6 {

	public static int answer() {

		int result = 0;

		// difference between the sum of the squares of the first 100 natural
		// numbers and the square of the sum
		int sumOfSquares = 0;
		for (int i = 1; i <= 100; i++) {
			sumOfSquares += Math.pow(i, 2);
		}

		//Sn = (n*(a1 + an))/2 -> ((1 + 100)*100)/2 -> 10100/2 -> 5050

		int sumSquared = (int) Math.pow((5050), 2);

		result = sumSquared - sumOfSquares;

		return result;

	}
}
