package com.theosirian.euler.problems;

public class Problem17 {

	public static final int ONE = 3, TWO = 3, THREE = 5, FOUR = 4, FIVE = 4, SIX = 3, SEVEN = 5, EIGHT = 5, NINE = 4,
			TEN = 3, ELEVEN = 6, TWELVE = 6, THIRTEEN = 8, FOURTEEN = 8, FIFTEEN = 7, SIXTEEN = 7, SEVENTEEN = 9,
			EIGHTEEN = 8, NINETEEN = 8, TWENTY = 6, THIRTY = 6, FORTY = 5, FIFTY = 5, SIXTY = 5, SEVENTY = 7,
			EIGHTY = 6, NINETY = 6, HUNDRED = 7, THOUSAND = 8, AND = 3;

	public static int answer() {

		int total = 0;

		for (int i = 1; i < 1001; i++) {

			StringBuilder reverse = new StringBuilder("" + i);
			reverse.reverse();

			if (reverse.length() == 1 || (reverse.length() >= 3 && Character.getNumericValue(reverse.charAt(1)) == 0)) {

				if (Character.getNumericValue(reverse.charAt(0)) == 1) {
					total += ONE;
				} else if (Character.getNumericValue(reverse.charAt(0)) == 2) {
					total += TWO;
				} else if (Character.getNumericValue(reverse.charAt(0)) == 3) {
					total += THREE;
				} else if (Character.getNumericValue(reverse.charAt(0)) == 4) {
					total += FOUR;
				} else if (Character.getNumericValue(reverse.charAt(0)) == 5) {
					total += FIVE;
				} else if (Character.getNumericValue(reverse.charAt(0)) == 6) {
					total += SIX;
				} else if (Character.getNumericValue(reverse.charAt(0)) == 7) {
					total += SEVEN;
				} else if (Character.getNumericValue(reverse.charAt(0)) == 8) {
					total += EIGHT;
				} else if (Character.getNumericValue(reverse.charAt(0)) == 9) {
					total += NINE;
				}
			}

			if (reverse.length() >= 2) {
				if (Character.getNumericValue(reverse.charAt(1)) != 1 && Character.getNumericValue(reverse.charAt(1)) != 0) {
					if (Character.getNumericValue(reverse.charAt(0)) == 1) {
						total += ONE;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 2) {
						total += TWO;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 3) {
						total += THREE;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 4) {
						total += FOUR;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 5) {
						total += FIVE;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 6) {
						total += SIX;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 7) {
						total += SEVEN;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 8) {
						total += EIGHT;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 9) {
						total += NINE;
					}
				}

				if (Character.getNumericValue(reverse.charAt(1)) == 1) {
					if (Character.getNumericValue(reverse.charAt(0)) == 1) {
						total += ELEVEN;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 2) {
						total += TWELVE;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 3) {
						total += THIRTEEN;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 4) {
						total += FOURTEEN;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 5) {
						total += FIFTEEN;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 6) {
						total += SIXTEEN;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 7) {
						total += SEVENTEEN;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 8) {
						total += EIGHTEEN;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 9) {
						total += NINETEEN;
					} else if (Character.getNumericValue(reverse.charAt(0)) == 0) {
						total += TEN;
					}
				} else if (Character.getNumericValue(reverse.charAt(1)) == 2) {
					total += TWENTY;
				} else if (Character.getNumericValue(reverse.charAt(1)) == 3) {
					total += THIRTY;
				} else if (Character.getNumericValue(reverse.charAt(1)) == 4) {
					total += FORTY;
				} else if (Character.getNumericValue(reverse.charAt(1)) == 5) {
					total += FIFTY;
				} else if (Character.getNumericValue(reverse.charAt(1)) == 6) {
					total += SIXTY;
				} else if (Character.getNumericValue(reverse.charAt(1)) == 7) {
					total += SEVENTY;
				} else if (Character.getNumericValue(reverse.charAt(1)) == 8) {
					total += EIGHTY;
				} else if (Character.getNumericValue(reverse.charAt(1)) == 9) {
					total += NINETY;
				}
			}

			if (reverse.length() >= 3) {
				if (!(i % 100 == 0)) {
					total += AND;
				}
				if (Character.getNumericValue(reverse.charAt(2)) == 1) {
					total += ONE + HUNDRED;
				} else if (Character.getNumericValue(reverse.charAt(2)) == 2) {
					total += TWO + HUNDRED;
				} else if (Character.getNumericValue(reverse.charAt(2)) == 3) {
					total += THREE + HUNDRED;
				} else if (Character.getNumericValue(reverse.charAt(2)) == 4) {
					total += FOUR + HUNDRED;
				} else if (Character.getNumericValue(reverse.charAt(2)) == 5) {
					total += FIVE + HUNDRED;
				} else if (Character.getNumericValue(reverse.charAt(2)) == 6) {
					total += SIX + HUNDRED;
				} else if (Character.getNumericValue(reverse.charAt(2)) == 7) {
					total += SEVEN + HUNDRED;
				} else if (Character.getNumericValue(reverse.charAt(2)) == 8) {
					total += EIGHT + HUNDRED;
				} else if (Character.getNumericValue(reverse.charAt(2)) == 9) {
					total += NINE + HUNDRED;
				}
			}

			if (reverse.length() >= 4) {
				if (Character.getNumericValue(reverse.charAt(3)) == 1) {
					total += ONE + THOUSAND;
				}
			}

			System.out.println(i + " added to " + total);

		}

		return total;

	}
}
