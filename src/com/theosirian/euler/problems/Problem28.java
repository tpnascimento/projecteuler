package com.theosirian.euler.problems;

/**
 * Created by SirOsirian on 29/12/13.
 */
public class Problem28 {

	public static int answer() {

		int sum = 1, number = 1;

		for (int i = 1; i < 1001; i += 2) {

			for (int j = 0; j < 4; j++) {

				number += (i + 1);
				sum += number;

			}

		}

		return sum;

	}

}
