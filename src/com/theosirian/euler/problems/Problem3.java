package com.theosirian.euler.problems;

import java.util.ArrayList;
import java.util.List;


public class Problem3 {

	public static String answer() {

		// largest prime factor of 600851475143

		long n = 600851475143l;

		List<Long> factors = new ArrayList<Long>();

		for (long i = 2; i <= n / i; i++) {

			while (n % i == 0) {
				factors.add(i);
				n /= i;
			}
		}
		if (n > 1) {
			factors.add(n);
		}
		return factors.get(factors.size() - 1).toString();
	}
}
