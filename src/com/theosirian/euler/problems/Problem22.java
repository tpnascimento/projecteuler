package com.theosirian.euler.problems;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by SirOsirian on 27/12/13.
 */
public class Problem22 {

	public static long answer() {

		try {

			String[] names = readNames("src/com/theosirian/extras/names.txt");
			names[0] = names[0].replace("\"", "");
			names[names.length - 1] = names[names.length - 1].replace("\"", "");
			List<String> sortedList = sortList(names);
			System.out.println(sortedList.toString());
			return scoreNames(sortedList);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return -1;

	}

	public static String[] readNames(String filename) throws IOException {
		FileReader fileReader = new FileReader(filename);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String names = bufferedReader.readLine();
		bufferedReader.close();

		return names.split("\",\"");
	}

	private static List<String> sortList(String[] unsortedNames) {

		Collection<String> c = new ArrayList<String>();

		List<String> sortedList = new ArrayList<String>(c);

		for (int i = 0; i < unsortedNames.length; i++) {

			sortedList.add(unsortedNames[i]);

		}

		Collections.sort(sortedList);
		return sortedList;
	}

	private static long scoreNames(List<String> names) {

		long total = 0, subTotal;

		for (int i = 1; i <= names.size(); i++) {

			subTotal = 0;
			String name = names.get(i - 1);

			for (int j = 0; j < name.length(); j++) {

				int letter = name.charAt(j) - 64;
				subTotal += letter;

			}

			subTotal *= i;
			total += subTotal;

		}

		return total;
	}

}
