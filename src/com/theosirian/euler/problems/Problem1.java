package com.theosirian.euler.problems;

public class Problem1 {

	public static int answer() {

		int result = 0;

		// multiples of 3 and 5 below 1000

		for (int i = 0; i < 1000; i++) {
			if (i % 3 == 0 || i % 5 == 0) {
				result += i;
			}
		}

		return result;

	}

}
