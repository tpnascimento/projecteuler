package com.theosirian.euler.problems;


public class Problem14 {

	private static int longestChain = 0, newChain = 0, producer = 0;

	public static String answer() {

		for (double i = 2; i < 1000000; i++) {
			newChain = chain(i);
			if (newChain > longestChain) {
				longestChain = newChain;
				producer = (int) i;
			}
		}
		return "The number " + producer + " produces the largest chain (" + longestChain + " numbers).";
	}

	public static int chain(double number) {

		int count = 0;

		while (number != 1) {

			if (number % 2 == 0) {
				number = number / 2;
			} else {
				number = (3 * number) + 1;
			}
			count++;
		}

		return count;
	}
}
