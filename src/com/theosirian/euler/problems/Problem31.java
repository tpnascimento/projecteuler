package com.theosirian.euler.problems;

/**
 * Created by SirOsirian on 29/12/13.
 */
public class Problem31 {

	public static final int ONE_PENCE = 200, TWO_PENCES = 100, FIVE_PENCES = 40, TEN_PENCES = 20, TWENTY_PENCES = 10, FIFTY_PENCES = 4,
			ONE_POUND = 2;

	public static int answer() {

		// i = 1p; j = 2p; k = 5p; l = 10p; m = 20p; n = 50p; o = 100;
		// starts at one because I won't make a whole for loop for 1 possible true value for 2 pounds coin
		int sum, diffWays = 1;

		for (int i = 0; i <= ONE_PENCE; i++) {

			for (int j = 0; j <= TWO_PENCES; j++) {

				for (int k = 0; k <= FIVE_PENCES; k++) {

					for (int l = 0; l <= TEN_PENCES; l++) {

						for (int m = 0; m <= TWENTY_PENCES; m++) {

							for (int n = 0; n <= FIFTY_PENCES; n++) {

								for (int o = 0; o <= ONE_POUND; o++) {

									sum = i + (j * 2) + (k * 5) + (l * 10) + (m * 20) + (n * 50) + (o * 100);

									if (sum == 200) {

										diffWays++;

									}

								}

							}

						}

					}

				}

			}

		}

		return diffWays;

	}

}
