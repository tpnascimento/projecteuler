package com.theosirian.euler.problems;

/**
 * Created by SirOsirian on 27/12/13.
 */
public class Problem21 {

	// d(a) = b and d(b) = a, where a ≠ b
	// amicable numbers under 10000

	public static int answer() {

		int total = 0;

		for (int i = 1; i < 10000; i++) {

			// d(i) = sum
			// d(sum) = i

			int b = divisorSum(i);
			int a = divisorSum(b);

			if (a == i && a != b) {
				System.out.println("D(" + i + ") = " + b + "; D(" + b + ") = " + a + ";");
				total += i;
			}

		}

		return total;
	}

	public static int divisorSum(int number) {

		int total = 0;

		for (int i = 1; i < number; i++) {

			if (number % i == 0) {

				total += i;

			}

		}

		return total;
	}

}
