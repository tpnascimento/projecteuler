package com.theosirian.euler.problems;

public class Problem10 {

	public static double answer() {

		// Find the sum of all the primes below two million.

		// It took a bit of time, the answer is 1.42913828922E11

		double result = 2;

		boolean isPrime;

		for (int n = 3; n < 2000000; n += 2) {
			isPrime = false;
			for (int i = 2; i < n; i++) {
				if (n % i > 0) {
					isPrime = true;
				} else {
					isPrime = false;
					break;
				}
			}
			if (isPrime) {
				result += n;
				System.out.println(result);
			}
		}

		return result;

	}
}
