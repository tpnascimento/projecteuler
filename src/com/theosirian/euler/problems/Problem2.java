package com.theosirian.euler.problems;

public class Problem2 {

	private static int a1, a2;

	public static int answer() {

		int result = 0;

		// sum of the even fibonacci below 4.000.000

		for (int i = 1; i < 4000000; i = nextFibonacci(i)) {

			if (i % 2 == 0) {

				result += i;

			}

		}

		return result;

	}

	public static int nextFibonacci(int number) {

		if (number == 1) {

			a1 = 1;
			return 2;

		}
		if (number == 2) {

			a1 = 2;
			return 3;

		} else {

			a2 = a1;
			a1 = number;
			return a2 + number;

		}

	}
}
