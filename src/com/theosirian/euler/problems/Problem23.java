package com.theosirian.euler.problems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SirOsirian on 27/12/13.
 */
public class Problem23 {

	public static long answer() {

		List<Integer> abundantNumbers = new ArrayList<Integer>();
		long sum = 0;

		for (int i = 0; i < 28123; i++) {

			if (divisorSum(i) > i) {
				abundantNumbers.add(i);
			}

		}

		boolean done;
		int number;
		for (int i = 0; i < 28123; i++) {

			done = false;

			for (int j = 0; j < abundantNumbers.size(); j++) {

				for (int k = 0; k < abundantNumbers.size(); k++) {

					number = abundantNumbers.get(k) + abundantNumbers.get(j);

					if (number > i) {
						break;
					}

					if (i == number) {

						done = true;
						break;

					}

				}

				if (done) {
					break;
				}
			}

			if (!done) {
				sum += i;
			}

		}

		return sum;
	}

	public static int divisorSum(int number) {

		int total = 0;

		for (int i = 1; i < number; i++) {

			if (number % i == 0) {

				total += i;

			}

		}

		return total;
	}


}
