package com.theosirian.euler;

import com.theosirian.euler.problems.Problem26;

public class Main {

	public static void main(String[] args) {
		System.out.println(Problem26.answer());
	}

}